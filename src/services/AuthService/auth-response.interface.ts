export interface SignUpInputData {
    name: string;
    email: string;
    password: string
}

export interface LoginInterface {
    email: string;
    password: string;
}

export interface AuthResponseInterface {
    token: string;
}