import http from '../http';
import {LoginInterface, SignUpInputData, AuthResponseInterface} from './auth-response.interface';

const signUp = async (data: SignUpInputData): Promise<AuthResponseInterface> => {
    const response = await http.post<AuthResponseInterface>('/auth', data);
    return response.data;
};

const login = async (data: LoginInterface): Promise<AuthResponseInterface> => {
    const response = await http.post<AuthResponseInterface>('/login', data);
    return response.data;
};

export {
    signUp,
    login
};