export interface AllTasksResponseInterface {
    id: string;
    title: string;
    description: string;
    dueDate: string;
    createdAt: string;
    categoryId: string
}

export interface InputData {
    title: string;
    description: string;
    dueDate: string;
    categoryId: string;
}