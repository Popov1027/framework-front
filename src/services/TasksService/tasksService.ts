import http from '../http';
import {AllTasksResponseInterface, InputData} from './tasks-response.interface';

const getAllTasks = async (page: number, limit: number): Promise<AllTasksResponseInterface[]> => {
    const response = await http.get<AllTasksResponseInterface[]>(`task?page=${page}&limit=${limit}`);
    return response.data;
};

const getTasksByCategoryId = async (categoryId: string): Promise<AllTasksResponseInterface[]> => {
    const response = await http.get<AllTasksResponseInterface[]>(`task/category/${categoryId}`);
    return response.data;
};

const getTaskById = async (id: string | undefined): Promise<AllTasksResponseInterface> => {
    const response = await http.get<AllTasksResponseInterface>(`task/${id}`);
    return response.data;
};

const createTask = async (inputData: InputData ): Promise<AllTasksResponseInterface> => {
    const response = await http.post<AllTasksResponseInterface>('task', inputData);
    return response.data;
};

const deleteTask = async (id: string):Promise<void> => {
    const response = await http.delete(`task/${id}`);
    return response.data;
};

const editTask = async (id: string | undefined, inputData:InputData):Promise<AllTasksResponseInterface> => {
    const response = await http.patch<AllTasksResponseInterface>(`/task/${id}`, inputData);
    return response.data;
};

export {
    getAllTasks,
    getTaskById,
    createTask,
    deleteTask,
    editTask,
    getTasksByCategoryId
};