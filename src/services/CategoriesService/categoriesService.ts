import http from '../http';
import {CategoriesResponseInterface} from './categories-response.interface';

const getAllCategories = async ():Promise<CategoriesResponseInterface[]> => {
    const response = await http.get<CategoriesResponseInterface[]>('category');
    return response.data;
};

const getCategoryById = async (id: string | undefined): Promise<CategoriesResponseInterface> => {
    const response = await http.get<CategoriesResponseInterface>(`category/${id}`);
    return response.data;
};

const createCategory = async (name: string): Promise<CategoriesResponseInterface> => {
    const response = await http.post<CategoriesResponseInterface>('category', {name});
    return response.data;
};

const deleteCategory = async (id: string): Promise<void> => {
    const response = await http.delete<void>(`category/${id}`);
    return response.data;
};


export {
    getAllCategories,
    getCategoryById,
    createCategory,
    deleteCategory,
};