export interface CategoriesResponseInterface {
    id: string;
    name: string;
}