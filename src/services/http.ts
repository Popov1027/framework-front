import axios from 'axios';

const http = axios.create({
    baseURL: 'http://localhost:3000/',
});

export default {
    get: http.get,
    post: http.post,
    put: http.put,
    delete: http.delete,
    patch: http.patch
};
