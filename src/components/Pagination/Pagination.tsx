import React, {FC} from 'react';
import {AllTasksResponseInterface} from '../../services/TasksService/tasks-response.interface';
import {BiLeftArrow, BiRightArrow} from 'react-icons/bi';

interface PaginationProps {
    limit: number;
    page: number;
    setPage: React.Dispatch<React.SetStateAction<number>>;
    tasks: AllTasksResponseInterface[]
}

const Pagination: FC<PaginationProps> = ({limit,setPage,page,tasks}) => {
    return (
        <div className="mt-8 flex space-x-2 font-bold text-white text-2xl">
            <button
                onClick={() => setPage(prevPage => Math.max(prevPage - 1, 1))}
                disabled={page === 1}
            >
                <BiLeftArrow/>
            </button>
            <p>{page}</p>
            <button
                onClick={() => setPage(prevPage => prevPage + 1)}
                disabled={tasks.length < limit}
            >
                <BiRightArrow/>
            </button>
        </div>
    );
};

export default Pagination;