import React, {FormEvent} from 'react';
import {CategoriesResponseInterface} from '../../services/CategoriesService/categories-response.interface';

export interface EditTaskFormInterface {
    title: string;
    setTitle: React.Dispatch<React.SetStateAction<string>>;
    description: string;
    setDescription: React.Dispatch<React.SetStateAction<string>>;
    dueDate: string;
    setDueDate: React.Dispatch<React.SetStateAction<string>>;
    handleEditTask(e: FormEvent): void;
    setCategoryId: React.Dispatch<React.SetStateAction<string>>;
    categories: CategoriesResponseInterface[]
}
