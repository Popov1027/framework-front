import React, {FC, FormEvent} from 'react';
import {AllTasksResponseInterface} from '../../services/TasksService/tasks-response.interface';
import moment from 'moment';
import {BsFillTrashFill} from 'react-icons/bs';
import {useNavigate} from 'react-router-dom';
import {BiEditAlt} from 'react-icons/bi';

interface TaskListInterface {
    tasks: AllTasksResponseInterface[];

    handleDeleteTask(id: string, e: FormEvent): void
}

const TaskList: FC<TaskListInterface> = ({
    tasks,
    handleDeleteTask
}) => {
    const navigate = useNavigate();

    return (
        <div className="flex flex-col mt-40 space-y-5">
            <div className="flex justify-between">
                <h1 className="text-white text-2xl font-bold">All tasks</h1>
                <button onClick={() => navigate('/add-task')}
                    className="rounded-full px-4 py-2 bg-[#242554] text-white border hover:bg-amber-500 transition duration-500 font-bold">
                    Add task
                </button>
            </div>
            {tasks.length !== 0 ?
                tasks.map((task) => (
                    <div
                        onClick={() => navigate(`${task.id}`)}
                        key={task.id}
                        className="flex justify-between px-12 bg-[#282A5A] shadow-xl rounded-xl h-32 w-[980px] p-3 hover:bg-amber-500 transition duration-300 cursor-pointer">
                        <div className="flex flex-col justify-center space-y-2">
                            <h1 className="text-white text-xl font-bold">{task.title}</h1>
                            <p className="text-indigo-500">Date limit: {moment(task.dueDate).format('llll')}</p>
                        </div>
                        <div className="flex flex-col justify-center space-y-4">
                            <button
                                type="button"
                                onClick={(event) => handleDeleteTask(task.id, event)}
                                className="text-2xl text-white hover:text-red-700 transition duration-500">
                                <BsFillTrashFill/>
                            </button>
                            <button
                                type="button"
                                onClick={(event) => {
                                    event.stopPropagation();
                                    navigate(`/edit-task/${task.id}`);
                                }}
                                className="text-2xl text-white hover:text-red-700 transition duration-500">
                                <BiEditAlt/>
                            </button>
                        </div>
                    </div>
                )) :
                <div className="flex justify-center items-center px-12 bg-[#282A5A] shadow-xl rounded-xl h-96 w-[980px] p-3">
                    <p className="text-white text-3xl font-bold">
                        There are no tasks
                    </p>
                </div>
            }
        </div>
    );
};

export default TaskList;