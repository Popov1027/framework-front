import React, {FormEvent} from 'react';
import {CategoriesResponseInterface} from '../../services/CategoriesService/categories-response.interface';

export interface CreateTaskFormInterface {
    title: string;
    description: string;
    dueDate: string;
    setTitle: React.Dispatch<React.SetStateAction<string>>;
    setDescription: React.Dispatch<React.SetStateAction<string>>;
    setDueDate: React.Dispatch<React.SetStateAction<string>>;
    handleCreateTask(e: FormEvent): void;
    categories: CategoriesResponseInterface[];
    setCategoryId: React.Dispatch<React.SetStateAction<string>>;
}
