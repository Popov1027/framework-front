import React, {FC} from 'react';
import {CreateTaskFormInterface} from './create-task.interface';

const CreateTaskForm: FC<CreateTaskFormInterface> = ({
    title,
    setTitle,
    setDescription,
    description,
    setDueDate,
    dueDate,
    handleCreateTask,
    categories,
    setCategoryId
}) => {
    return (
        <div className="flex justify-center">
            <div className="flex flex-col mt-40 px-12 bg-[#282A5A] shadow-xl rounded-xl w-[980px] p-3">
                <div className="flex justify-center text-white text-3xl font-bold">
                    <h1>Create task</h1>
                </div>
                <form onSubmit={handleCreateTask} className="flex flex-col mt-3 w-full">
                    <div className="mb-6">
                        <label className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Title*</label>
                        <input
                            type="text"
                            value={title}
                            onChange={(e) => setTitle(e.target.value)}
                            className="bg-indigo-900 text-white text-sm rounded-lg block w-full p-2.5 focus-visible:outline-0"
                        />
                    </div>
                    <div className="mb-6">
                        <label className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Description*</label>
                        <textarea
                            value={description}
                            onChange={(e) => setDescription(e.target.value)}
                            className="bg-indigo-900 text-white text-sm rounded-lg block w-full p-2.5 focus-visible:outline-0"
                        />
                    </div>
                    <div className="mb-6">
                        <label className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Date limit*</label>
                        <input
                            type="date"
                            value={dueDate}
                            onChange={(e) => setDueDate(e.target.value)}
                            className="bg-indigo-900 text-white text-sm rounded-lg block w-full p-2.5 focus-visible:outline-0"
                        />
                    </div>
                    <div className="mb-6">
                        <label className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Select a category*</label>
                        <select onChange={(e) => setCategoryId(e.target.value)} className="bg-indigo-900 text-white text-sm rounded-lg block w-full p-2.5 focus-visible:outline-0">
                            {categories.map((category) => (
                                <option key={category.id} value={category.id}>
                                    {category.name}
                                </option>
                            ))}
                        </select>

                    </div>
                    <div className="flex justify-end my-4">
                        <button
                            className="w-40 rounded-full px-4 py-2 bg-[#242554] text-white border hover:bg-amber-500 transition duration-500 font-bold">
                            Submit
                        </button>
                    </div>
                </form>
            </div>
        </div>
    );
};

export default CreateTaskForm;