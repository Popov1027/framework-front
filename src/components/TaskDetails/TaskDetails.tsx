import React, {FC} from 'react';
import {AllTasksResponseInterface} from '../../services/TasksService/tasks-response.interface';
import moment from 'moment';
import {CategoriesResponseInterface} from '../../services/CategoriesService/categories-response.interface';
import {useNavigate} from 'react-router-dom';

interface TaskDetailsInterface {
    task: AllTasksResponseInterface | undefined;
    category: CategoriesResponseInterface | undefined
}

const TaskDetails: FC<TaskDetailsInterface> = ({task, category}) => {
    const navigate = useNavigate();
    
    return (
        <div className="flex justify-center">
            <div className="flex flex-col mt-40 px-12 bg-[#282A5A] shadow-xl rounded-xl h-96 w-[980px] p-3">
                <div className="flex justify-between w-full">
                    <div className="flex flex-col mt-12 space-y-1">
                        <h1 className="text-white text-3xl font-bold">{task?.title}</h1>
                        <p className="text-indigo-300 text-sm">Created at: {moment(task?.createdAt).format('LLL')}</p>
                        <p className="text-indigo-300 text-sm">{category?.name}</p>
                    </div>
                    <div>
                        <p className='text-white font-bold mt-2'>Date limit: {moment(task?.dueDate).format('lll')}</p>
                    </div>
                </div>
                <div className="h-full mt-12 space-y-12">
                    <p className="text-white text-2xl">{task?.description}</p>
                </div>
                <div className="flex justify-end">
                    <button onClick={() => navigate(-1)} className="w-40 rounded-full px-4 py-2 bg-[#242554] text-white border hover:bg-amber-500 transition duration-500 font-bold">
                        Back
                    </button>
                </div>
            </div>
        </div>
    );
};

export default TaskDetails;