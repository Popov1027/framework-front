import React, {ChangeEvent, FC} from 'react';

interface InputProps {
    id: string;
    name: string;
    value: string;
    type: string
    onChange(e: ChangeEvent<HTMLInputElement>): void
}

const Input:FC<InputProps> = ({ id, name, value, onChange, type }) => {
    return (
        <div className="space-y-2">
            <label htmlFor={id} className="text-white font-bold text-lg px-1">
                {name}*
            </label>
            <input
                id={id}
                name={name}
                value={value}
                onChange={onChange}
                type={type}
                className="pl-5 bg-indigo-900 text-white text-md rounded-full block w-full p-2.5 focus-visible:outline-0"
            />
        </div>
    );
};

export default Input;
