import React, {ChangeEvent, FC} from 'react';
import {InputFieldsInterface, SignUpProps} from './sign-up.interface';
import Input from './Input';

const SignUpForm: FC<SignUpProps> = ({name, email, password, setName, setEmail, setPassword, handleSubmit}) => {
    const inputFields: InputFieldsInterface[] = [
        {
            id: 'name',
            name: 'Name',
            value: name,
            onChange: (e: ChangeEvent<HTMLInputElement>) => setName(e.target.value),
            type: 'text',
        },
        {
            id: 'email',
            name: 'Email',
            value: email,
            onChange: (e: ChangeEvent<HTMLInputElement>) => setEmail(e.target.value),
            type: 'email',
        },
        {
            id: 'password',
            name: 'Password',
            value: password,
            onChange: (e: ChangeEvent<HTMLInputElement>) => setPassword(e.target.value),
            type: 'password',
        },
    ];

    return (
        <div className="flex justify-center items-center h-screen">
            <div className="backdrop-blur-[25px] bg-indigo-600/10 w-1/3 px-12 py-10 rounded-lg">
                <div className="flex justify-center items-center">
                    <p className="text-white text-3xl font-bold">Sign Up</p>
                </div>
                <form className="mt-6 space-y-6" onSubmit={handleSubmit}>
                    {inputFields.map((input) => (
                        <Input
                            id={input.id}
                            key={input.id}
                            value={input.value}
                            name={input.name}
                            onChange={input.onChange}
                            type={input.type}
                        />
                    ))}
                    <div className="flex justify-center mt-12">
                        <button
                            type="submit"
                            className="w-40 rounded-full px-4 py-2 bg-[#242554] text-white border hover:bg-amber-500 transition duration-500 font-bold"
                        >
                            Sign Up
                        </button>
                    </div>
                </form>
            </div>
        </div>
    );
};

export default SignUpForm;
