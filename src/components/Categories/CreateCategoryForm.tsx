import React, {FC, FormEvent} from 'react';
import {AiOutlinePlusCircle} from 'react-icons/ai';

interface CreateCategoryFormInterface {
    name: string
    isVisible: boolean;
    handleCreateCategory(e: FormEvent): void
    setName: React.Dispatch<React.SetStateAction<string>>;
    setIsVisible: React.Dispatch<React.SetStateAction<boolean>>;
}

const CreateCategoryForm:FC<CreateCategoryFormInterface> = ({isVisible, setIsVisible, handleCreateCategory, name, setName}) => {
    return (
        <>
            {!isVisible ?
                <button onClick={() => setIsVisible(true)}>
                    <AiOutlinePlusCircle className="text-3xl text-white hover:scale-125 transition"/>
                </button> :
                <form onSubmit={handleCreateCategory} className="w-full space-y-2 flex flex-col justify-center">
                    <input
                        type="text"
                        value={name}
                        onChange={(e) => setName(e.target.value)}
                        className="bg-indigo-900 text-white text-sm rounded-full block w-full p-2.5 focus-visible:outline-0"
                    />
                    <button type="submit" className="rounded-full px-4 py-2 bg-[#242554] text-white border hover:bg-amber-500 transition duration-500 font-bold">
                        Create
                    </button>
                </form>
            }  
        </>
    );
};

export default CreateCategoryForm;