import React, {FC, FormEvent, useState} from 'react';
import {CategoriesResponseInterface} from '../../services/CategoriesService/categories-response.interface';
import CreateCategoryForm from './CreateCategoryForm';
import {createCategory, deleteCategory} from '../../services/CategoriesService/categoriesService';
import {toast} from 'react-toastify';
import List from './List';

interface CategoriesListProps {
    categories: CategoriesResponseInterface[];
    setCategories: React.Dispatch<React.SetStateAction<CategoriesResponseInterface[]>>;
    selectedCategory(categoryId: string): void;
    selectedCategoryId: string
}

const CategoriesList: FC<CategoriesListProps> = ({categories, setCategories, selectedCategory, selectedCategoryId}) => {
    const [isVisible, setIsVisible] = useState<boolean>(false);
    const [name, setName] = useState<string>('');

    const handleCreateCategory = (e: FormEvent) => {
        e.preventDefault();
        createCategory(name)
            .then((data) => {
                setCategories((prevCategories) => [...prevCategories, data]);
                toast.success('The category has been created');
                setIsVisible(false);
            })
            .catch(() => toast.error('The category wasn\'t created'));
    };
    
    const handleDeleteCategory = (e: FormEvent, id: string) => {
        e.stopPropagation();
        deleteCategory(id)
            .then(() => {
                setCategories((prevCategory) => prevCategory.filter((category) => category.id !== id));
                toast.success('The category has been deleted');
            })
            .catch(() => toast.error('The category wasn\'t created'));
    };

    return (
        <div className="space-y-3 mr-5 flex flex-col justify-center items-center h-[750px]">
            <List
                handleDeleteCategory={handleDeleteCategory}
                selectedCategoryId={selectedCategoryId}
                selectedCategory={selectedCategory}
                categories={categories}
            />
            <CreateCategoryForm
                name={name}
                setName={setName}
                isVisible={isVisible}
                setIsVisible={setIsVisible}
                handleCreateCategory={handleCreateCategory}
            />
        </div>
    );
};

export default CategoriesList;