import React, {FC, FormEvent, useState} from 'react';
import {CategoriesResponseInterface} from '../../services/CategoriesService/categories-response.interface';
import {BsTrashFill} from 'react-icons/bs';

interface ListInterface {
    categories: CategoriesResponseInterface[];
    selectedCategory(categoryId: string): void;
    selectedCategoryId: string;
    handleDeleteCategory(e: FormEvent, id: string): void;
}

const List: FC<ListInterface> = ({categories,selectedCategory, selectedCategoryId, handleDeleteCategory}) => {
    const [hoverCategoryId, setHoverCategoryId] = useState<string>('');

    return (
        <>
            {categories.map((category) => (
                <div
                    key={category.id}
                    onMouseEnter={() => setHoverCategoryId(category.id)}
                    onMouseLeave={() => setHoverCategoryId('')}
                    onClick={() => selectedCategory(category.id)}
                    className={`w-60 relative flex items-center justify-between rounded-full px-4 py-2 text-white border cursor-pointer relative ${
                        category.id === selectedCategoryId ? 'bg-amber-500 font-bold' : 'bg-[#242554] hover:bg-amber-500 transition duration-500'
                    }`}
                >
                    <p>{category.name}</p>
                    <div className={`absolute right-3 ${hoverCategoryId === category.id ? 'flex' : 'hidden'} text-xl`}>
                        <BsTrashFill
                            onClick={(e) => handleDeleteCategory(e, category.id)}
                            className="hover:text-red-700 transition duration-400"
                        />
                    </div>
                </div>
            ))}
        </>
    );
};

export default List;