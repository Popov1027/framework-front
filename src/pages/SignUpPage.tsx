import React, {FormEvent, useState} from 'react';
import SignUpForm from '../components/AuthComponents/SignUpForm';
import {signUp} from '../services/AuthService/authService';
import {useNavigate} from 'react-router-dom';

const SignUpPage = () => {
    const [name, setName] = useState<string>('');
    const [email, setEmail] = useState<string>('');
    const [password, setPassword] = useState<string>('');
    const navigate = useNavigate();

    const handleSubmit = (e: FormEvent) => {
        e.preventDefault();
        signUp({name, email, password}).then(({token}) => {
            localStorage.setItem('token', token);
            navigate('/');
        });
    };
    
    return (
        <SignUpForm
            name={name}
            email={email}
            password={password}
            setName={setName}
            setEmail={setEmail}
            setPassword={setPassword}
            handleSubmit={handleSubmit}
        />
    );
};

export default SignUpPage;