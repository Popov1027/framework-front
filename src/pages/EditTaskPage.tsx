import React, {FormEvent, useEffect, useState} from 'react';
import EditTaskForm from '../components/EditTaskForm/EditTaskForm';
import {useParams} from 'react-router-dom';
import {editTask, getTaskById} from '../services/TasksService/tasksService';
import {toast} from 'react-toastify';
import {getAllCategories} from '../services/CategoriesService/categoriesService';
import {CategoriesResponseInterface} from '../services/CategoriesService/categories-response.interface';

const EditTaskPage = () => {
    const [title, setTitle] = useState<string>('');
    const [description, setDescription] = useState<string>('');
    const [dueDate, setDueDate] = useState<string>('');
    const [isLoading, setIsLoading] = useState<boolean>(false);
    const [categoryId, setCategoryId] = useState<string>('');
    const [categories, setCategories] = useState<CategoriesResponseInterface[]>([]);
    const {id} = useParams();

    useEffect(() => {
        getTaskById(id).then(({title, dueDate, description, categoryId}) => {
            setTitle(title);
            setDescription(description);
            setDueDate(dueDate);
            setCategoryId(categoryId);
            setIsLoading(true);
        });
        getAllCategories().then((data) => setCategories(data));
    }, []);

    const handleEditTask = (e: FormEvent) => {
        e.preventDefault();
        editTask(id, {title, dueDate, description, categoryId})
            .then(() => toast.success('The task has been edited'))
            .catch(() => toast.error('The task wasn\'t edtide'));
    };

    return (
        <>
            {isLoading &&
                <EditTaskForm
                    title={title}
                    description={description}
                    dueDate={dueDate}
                    setTitle={setTitle}
                    setDescription={setDescription}
                    setDueDate={setDueDate}
                    handleEditTask={handleEditTask}
                    setCategoryId={setCategoryId}
                    categories={categories}
                />}
        </>
    );
};

export default EditTaskPage;