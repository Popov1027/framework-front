import React, {useEffect, useState} from 'react';
import {useParams} from 'react-router-dom';
import {getTaskById} from '../services/TasksService/tasksService';
import {AllTasksResponseInterface} from '../services/TasksService/tasks-response.interface';
import TaskDetails from '../components/TaskDetails/TaskDetails';
import {CategoriesResponseInterface} from '../services/CategoriesService/categories-response.interface';
import {getCategoryById} from '../services/CategoriesService/categoriesService';

const TaskPage = () => {
    const [task, setTask] = useState<AllTasksResponseInterface>();
    const [category, setCategory] = useState<CategoriesResponseInterface>();
    const [isLoading, setIsLoading] = useState<boolean>(false);
    const {id} = useParams();

    useEffect(() => {
        getTaskById(id).then((data) => {
            setTask(data);
            setIsLoading(true);
        });
    }, []);
    
    useEffect(() => {
        if (task?.categoryId) {
            getCategoryById(task.categoryId).then((data) => setCategory(data));
        }
    }, [task?.categoryId]);
    
    return (
        <>
            {isLoading && <TaskDetails task={task} category={category}/>}
        </>
    );
};

export default TaskPage;