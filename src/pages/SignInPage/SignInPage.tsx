import React, {FormEvent, useState} from 'react';
import {useNavigate} from 'react-router-dom';
import {login} from '../../services/AuthService/authService';
import SignInForm from '../../components/AuthComponents/SignInForm';
import {toast} from 'react-toastify';

const SignUpPage = () => {
    const [email, setEmail] = useState<string>('');
    const [password, setPassword] = useState<string>('');
    const navigate = useNavigate();

    const handleSubmit = (e: FormEvent) => {
        e.preventDefault();
        login({email, password})
            .then(({token}) => {
                localStorage.setItem('token', token);
                navigate('/');
            })
            .catch(() => toast.error('Invalid credentials'));
    };

    return (
        <SignInForm
            email={email}
            password={password}
            setEmail={setEmail}
            setPassword={setPassword}
            handleSubmit={handleSubmit}
        />
    );
};

export default SignUpPage;