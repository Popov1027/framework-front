import React, { FormEvent, useEffect, useState } from 'react';
import { toast } from 'react-toastify';
import TaskList from '../components/TaskList/TaskList';
import { AllTasksResponseInterface } from '../services/TasksService/tasks-response.interface';
import {deleteTask, getAllTasks, getTasksByCategoryId} from '../services/TasksService/tasksService';
import CategoriesList from '../components/Categories/CategoriesList';
import {getAllCategories} from '../services/CategoriesService/categoriesService';
import {CategoriesResponseInterface} from '../services/CategoriesService/categories-response.interface';
import Pagination from '../components/Pagination/Pagination';

const TasksPage = () => {
    const [tasks, setTasks] = useState<AllTasksResponseInterface[]>([]);
    const [categories, setCategories] = useState<CategoriesResponseInterface[]>([]);
    const [isLoading, setIsLoading] = useState<boolean>(false);
    const [selectedCategoryId, setSelectedCategoryId] = useState<string>('');
    const [page, setPage] = useState<number>(1);
    const limit: number = 3;

    useEffect(() => {
        getAllTasks(page, limit).then((data) => {
            setTasks(data);
            setIsLoading(true);
        });
        getAllCategories().then((data) => {
            setCategories(data);
            setIsLoading(true);
        });
    }, [page, limit]);

    const handleDeleteTask = (id: string, e: FormEvent) => {
        e.stopPropagation();
        deleteTask(id)
            .then(() => {
                toast.success('The task has been deleted');
                setTasks((prevTasks) => prevTasks.filter((task) => task.id !== id));
            })
            .catch(() => toast.error('The task wasn\'t deleted'));
    };

    const selectedCategory = (categoryId: string) => {
        setSelectedCategoryId(categoryId);
        getTasksByCategoryId(categoryId).then((data) => setTasks(data));
    };

    return (
        <div className="flex justify-center">
            {isLoading && (
                <>
                    <CategoriesList
                        selectedCategoryId={selectedCategoryId}
                        selectedCategory={selectedCategory}
                        categories={categories}
                        setCategories={setCategories}
                    />
                    <div className="flex flex-col h-screen items-center">
                        <TaskList
                            tasks={tasks}
                            handleDeleteTask={handleDeleteTask}
                        />
                        <Pagination limit={limit} setPage={setPage} page={page} tasks={tasks}/>
                    </div>
                </>
            )}
        </div>
    );
};

export default TasksPage;
