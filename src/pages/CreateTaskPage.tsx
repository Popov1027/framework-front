import React, {FormEvent, useEffect, useState} from 'react';
import CreateTaskForm from '../components/CreateTaskForm/CreateTaskForm';
import {createTask} from '../services/TasksService/tasksService';
import {useNavigate} from 'react-router-dom';
import { toast } from 'react-toastify';
import {CategoriesResponseInterface} from '../services/CategoriesService/categories-response.interface';
import {getAllCategories} from '../services/CategoriesService/categoriesService';

const CreateTaskPage = () => {
    const [title, setTitle] = useState<string>('');
    const [description, setDescription] = useState<string>('');
    const [dueDate, setDueDate] = useState<string>('');
    const [categories, setCategories] = useState<CategoriesResponseInterface[]>([]);
    const [categoryId, setCategoryId] = useState<string>('');
    const navigate = useNavigate();

    useEffect(() => {
        getAllCategories().then((data) => setCategories(data));
    },[]);

    const handleCreateTask = (e: FormEvent) => {
        e.preventDefault();
        createTask({
            title,
            dueDate,
            description,
            categoryId
        }).then(() => {
            navigate('/');
            toast.success('The task has been created');
        }).catch(() => toast.error('The task was not created'));
    };

    return (
        <CreateTaskForm
            title={title}
            description={description}
            dueDate={dueDate}
            setTitle={setTitle}
            setDescription={setDescription}
            setDueDate={setDueDate}
            handleCreateTask={handleCreateTask}
            categories={categories}
            setCategoryId={setCategoryId}
        />
    );
};

export default CreateTaskPage;