import React, {useEffect} from 'react';
import {Route, Routes, useNavigate} from 'react-router-dom';
import TasksPage from './pages/TasksPage';
import TaskPage from './pages/TaskPage';
import CreateTaskPage from './pages/CreateTaskPage';
import 'react-toastify/dist/ReactToastify.css';
import { ToastContainer } from 'react-toastify';
import EditTaskPage from './pages/EditTaskPage';
import SignUpPage from './pages/SignUpPage';
import SignInPage from './pages/SignInPage/SignInPage';

function App() {
    const token = localStorage.getItem('token');
    const navigate = useNavigate();
    
    useEffect(() => {
        if (!token && window.location.pathname !== '/sign-up') {
            navigate('/sign-in');
        }
    },[]);
    
    return (
        <>
            <ToastContainer />
            <Routes>
                <Route path={'/'} element={<TasksPage/>}/>
                <Route path={'/:id'} element={<TaskPage/>}/>
                <Route path={'/add-task'} element={<CreateTaskPage/>}/>
                <Route path={'/edit-task/:id'} element={<EditTaskPage/>}/>
                <Route path={'/sign-up'} element={<SignUpPage/>}/>
                <Route path={'/sign-in'} element={<SignInPage/>}/>
            </Routes>
        </>
    );
}

export default App;
